#include <iostream>
#include "Pila.h"
using namespace std;

// Funcion que serviara para guardar la posicion del contenedor a eliminar.
void quitar(string **&pila, int pilas, int contenedores, string eliminacion, int &eliminar_i, int &eliminar_j, int posicion_i, string container, int tope[]){
  for (int i=0; i<pilas; i++){
    for (int j=0; j<contenedores; j++){
      if (pila[i][j] == eliminacion){
        eliminar_i = i;
        eliminar_j = j;
      }
    }
  }
}

// Funcion menu que llamara a las otras funciones.
void menu(string **pila, int tope[], int contenedores, int pilas){
  // Creacion de variables.
  int opcion;
  int posicion_i;
  string letra;
  string container;
  string eliminacion;
  Pila p = Pila();
  
  do {
    cout << "---------------------------" << endl;
    cout << "1. Agregar/push" << endl;
    cout << "2. Remover/pop" << endl;
    cout << "3. salir" << endl;
    cout << "---------------------------" << endl;
    
    cout << "\nIngrese una opcion: ";
    cin >> opcion;
    
    switch (opcion) {
    case 1:
      // se especifica la pila que se llenara con el container
      cout << "Ingrese la pila: " << endl;
      cin >> posicion_i;
      cout << "Ingrese el nombre del container (3 caracteres): " << endl;
      cin >> container;
      // Se llama a push para rellenar un espacio vacio por un container
      p.push(pila, tope[posicion_i], contenedores, posicion_i, container, pilas);
      tope[posicion_i]++;
      break;
      
    case 2:
      int eliminar_i;
      int eliminar_j;
      cout << "Ingrese el container que desea remover: " << endl;
      cin >> eliminacion;
      /* Se llama a las siguientes funciones las cuales 
      recorreran las pilas e iran eliminando el container que se especifique.*/
      quitar(pila, pilas, contenedores, eliminacion, eliminar_i, eliminar_j, posicion_i, container, tope);
      p.remover(pila, pilas, contenedores, eliminacion, eliminar_j, eliminar_i, tope, posicion_i);
      p.pop(pila, tope[posicion_i], eliminacion, pilas, contenedores,eliminar_i, eliminar_j);
      tope[eliminar_i]--;
      p.ver_pila(pila, pilas, contenedores);
      break;
    }        
  } while (opcion != 3);
}

// Main
int main() {
  // Creacion de variables
  int contenedores;
  int pilas;
  // Se lllama a la clase Pila
  Pila p = Pila();
  
  // Se ingresa el numero de contenedores.
  cout << "Ingrese el numero de contenedores: " << endl;
  cin >> contenedores;
  // Se ingresa el numero de pilas.
  cout << "Ingrese el numero de pilas: " << endl;
  cin >> pilas;
  // Se crea un tope con el numero de pilas
  int tope[pilas];
  // Se igualan todos los topes a 0.
  for (int i=0; i<pilas; i++){
    tope[i] = 0;
  }
  // Si el numero de contenedores es es mayor a 0.
  if (contenedores > 0){
    // Se crea la pila con el tamaño correspondiente.
    string **pila = new string*[pilas];
    for(int i=0; i<pilas;i++){
      pila[i] = new string[contenedores];
    }
  
    // Se llama al menu.
    p.rellenar(pila, pilas, contenedores);
    menu(pila, tope, contenedores, pilas);
  }
  return 0;
}

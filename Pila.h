#include <iostream>
using namespace std;

#ifndef PILA_H
#define PILA_H


class Pila {
  
  // Metodos privados
  private:
    int numero = 0;
    bool band;
    int tope = 0;
    
  // Metodos publicos
  public:
    Pila();
    Pila(int numero, bool band, int tope);
    
    // Funciones
    void pila_vacia(int tope);
    void pila_llena(int tope, int contenedores);
    void push(string **pila, int &tope, int contenedores, int pila_cambiar, string &container, int pilas);
    void pop(string **pila, int &tope, string eliminacion, int pilas, int contenedores, int eliminar_i, int eliminar_j);
    void rellenar(string **pila, int pilas, int contenedores);
    void ver_pila(string **pila, int pilas, int contenedores);
    void remover(string **&pila, int pilas, int contenedores, string eliminacion, int eliminar_j, int eliminar_i, int tope[],int posicion_i);
  
  
  
  
};
#endif
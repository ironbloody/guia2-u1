**Guia 2 Unidad 1**                                                                                             
Programa basado en programación orientada a objetos y arreglos.                                                                           

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                                
Make                                                                                                                                             
Ubuntu                                                                                                                                                                                                                                                                            
**Instalación**                                                                                                                                       
Instalar Make si no esta en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make

**Problematica**                                                                                                                                       
Se pide crear un puerto que guarde mercaderia en contenedores. En este se debe gestionar el ingreso y salida de contenedores. Si al retirar un contenedor, este tiene otro arriba de el, hay que moverlo y colocarlo en otra pila.                                                                    

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal lo siguiente: ./programa.                    
Una vez abierto el progroma este le pedira ingresar el numero de contenedores y numeros de pilas para asi poder crear el puerto, luego de esto el      programa le mostrara un menu con 3 opciones.                                                                                                           
                                                                                                                                                       
La primera opcion le permitiria ingresar mercaderia a los contenedores dandole un nombre a este.                                                       
Porfavor el nombre del contenedor debe ser de 3 caracteres y no pueden haber contenedores con el mismo nombre.                                         
                                                                                                                                                       
La segunda opcion le permitira sacar un contenedor, al seleccionar este opcion el programa le preguntara que contenedor desea quitar, si intenta sacar uno y arriba de este hay mas contenedores el programa le preguntara donde desea mover los demas, una vez movidos los otros contenedores se quitara el  contenedor.                                                                                                                                            
                                                                                                                                                       
La tercera y ultima opcion le permitira salir del programa.                                                                                            
                                                                                                                                                        
**Construido con**                                                                                                                                    
C++                                                                                                                                      
                                                                                                                                         
Librerias:                                                                                                                               
Iostream                                                                                                                              

**Versionado**                                                                                                                                        
Version 1.6                                                                                                                                        

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                                                                               





#include <iostream>
#include "Pila.h"
using namespace std;


Pila::Pila(){
  int numero = 0;
  bool band;
  int tope;
}

Pila::Pila(int numero, bool band, int tope){
  this -> numero = numero;
  this -> band = band;
  this -> tope = tope;
}



// Funcion que verifica si la pila esta vacia o no.
void Pila::pila_vacia(int tope){
  if (tope == 0){ 
    // La pila esta vacia
    band = true;
  }
  else{
    // La pila no esta vacia
    band = false;
  }
}

// Funcion que verifica si la pila esta llena o no.
void Pila::pila_llena(int tope, int contenedores){
  if (tope == contenedores){
    // La pila esta llena
    band = true;
  }
  else{
    // La pila no esta llena
    band = false;
  }
}

// Funcion push que permitira el ingreso de elementos a la pila.
void Pila::push(string **pila, int &tope, int contenedores, int pila_cambiar, string &container, int pilas){
  pila_llena(tope, contenedores);
  // Si la pila no esta llena se agregaran elementos.
  if(this -> band == true){
    cout << "Desbordamiento, pila llena" << endl;
  }
  else{
    pila[tope][pila_cambiar] = container;
    this -> tope = tope;
  }
  ver_pila(pila, pilas, contenedores);
}

// Funcion pop que permitira la eliminacion de elementos en la pila.
void Pila::pop(string **pila, int &tope, string eliminacion, int pilas, int contenedores, int eliminar_i, int eliminar_j){
  
    pila[eliminar_i][eliminar_j] = "   ";
    tope--;
}

// Funcion que rellenara toda la matriz con espacios vacios.
void Pila::rellenar(string **pila, int pilas, int contenedores){
  for (int i=0; i<pilas; i++){
    for (int j=0; j<contenedores; j++){
      pila[i][j] = "   ";
    }
  }
}

// Funcion que imprime la pila.
void Pila::ver_pila(string **pila, int pilas, int contenedores){
  for (int i=pilas-1; i>=0; i--){
    for (int j=0; j<contenedores; j++){
      cout << "|" << pila[i][j] << "|";
    }
    cout << endl;
  }
}

// Funcion que permite eliminar y mover contenedores si es que estan arriba del que se desea.
void Pila::remover(string **&pila, int pilas, int contenedores, string eliminacion, int eliminar_j, int eliminar_i, int tope[], int posicion_i){
  // Creacion variable.
  string nombre_mover;
  // Se reccore las pilas.
  for(int i=pilas-1;i>=eliminar_i;i--){
    if(pila[i][eliminar_j] != eliminacion && pila[i][eliminar_j] != "   "){
      int pila_mover;
      nombre_mover = pila[i][eliminar_j];
      // Si el container que se desea mover no esta vacio.
      cout << "A que pila desea mover el contenedor? pila:  " << nombre_mover << endl;
      cin >> pila_mover;
      int top = tope[pila_mover];
      // Se llama a push.
      push(pila, top, contenedores, pila_mover, nombre_mover, pilas);
      pila[i][eliminar_j] = "   ";
      tope[eliminar_i]--;
      tope[pila_mover]++;
      // Se llama a pop.
      cout << "\n";
    }
  }
}
